package TestPages;

import Globales.CapturaPantalla;
import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public class PagoTarjetasCorporativas_24online {

    private Map<String, Object> vars;
    JavascriptExecutor js;
    CapturaPantalla print = new CapturaPantalla();

      public void click_link_24online()
    {
        Util.driver.findElement(By.linkText("24online")).click();
    }
    public void click_link_tarjetacredito()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(1);
        Util.waitForElementToBeClickable(By.id("1002"));
        WebElement carga = Util.driver.findElement(By.id("1002"));
        String actual = carga.getText();
        Util.assert_contiene("SUBMENU", "Verificación de etiqueta", actual, "Tarjeta", false, "N");
        //Util.assert_contiene();
        carga.click();
    }

    public void click_link_tarjetacorporativa()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(2);
        WebElement carga = Util.driver.findElement(By.linkText("Tarjetas Corporativas - Franquiciados"));
        String actual = carga.getText();
        Util.assert_contiene("SUBMENU","Verificacion de etiqueta",actual,"Tarjetas Corporativas - Franquiciados",false,"N");
        carga.click();
    }

    public void vp_posicion_consolidada()
    {
        boolean mensaje= Util.driver.getPageSource().contains("Seleccione una tarjeta y la operación que desea realizar");
        String actual = "";
        if (mensaje=true) actual="Ok posición consolidada";
        else
            actual="Fallido";
        Util.assert_contiene("PAGO DE TARJETAS", "Verificación de posición consolidada tarjetas corporativas", actual,"Ok posición consolidada", true,"N");

    }

    public void selecciona_tarjeta(String tarjeta_act, String select_tar)
    {
        WebElement tarjeta =  Util.driver.findElement(By.cssSelector(select_tar));
        tarjeta.click();
        Reporte.agregarPaso("PAGO DE TARJETAS","Tarjeta seleccionada ",tarjeta_act,"",false,"N");

    }
    public void selecciona_pago()
    {
        //Selecciona submenu pago
        WebElement selec =Util.driver.findElement(By.cssSelector("li:nth-child(2)"));
        String actual = selec.getText();
        Util.assert_contiene("PAGO DE TARJETAS","Verificacion de etiqueta",actual,"Pago",true,"N");
        selec.click();
    }
    public void selec_cuentadebito(String cuenta_act,String selcuenta)
    {
        //SELECCIONA UNA OPCION DE UNA LISTA
        Select empresa = new Select(Util.driver.findElement(By.name("selectCuentas")));
        empresa.selectByValue(selcuenta);
        Reporte.agregarPaso("PAGO DE TARJETAS","Cuenta seleccionada para cobro de comisión",cuenta_act,"",false,"N");
    }
    public void ingresa_monto(String selmonto)
    {
        WebElement valor=Util.driver.findElement(By.name("txt_monto"));
        valor.click();
        valor.sendKeys(selmonto);
        Reporte.agregarPaso("PAGO DE TARJETAS","Ingresa valor a pagar",selmonto,"",false,"N");
    }
    public void ingresa_concepto(String selconcepto)
    {
        WebElement concepto=Util.driver.findElement(By.name("txt_concepto"));
        concepto.click();
        concepto.sendKeys(selconcepto);
        Reporte.agregarPaso("PAGO DE TARJETAS","Ingresa concepto",selconcepto,"",false,"N");
    }
    public void click_continuar()
    {
        Util.AvanzarPagina();
        WebElement concepto=Util.driver.findElement(By.name("button"));
        String actual = concepto.getAttribute("value");
        Util.assert_igual("PAGO DE TARJETAS", "Click en botón Continuar", actual, "Continuar", true, "N");
        concepto.click();
    }

    public void ingresa_token(String seltoken)
    {
        Util.AvanzarPagina();
        WebElement token=Util.driver.findElement(By.name("txt_token"));
        token.click();
        token.sendKeys(seltoken);
        Reporte.agregarPaso("PAGO DE TARJETAS","Ingresa token",seltoken,"",false,"N");
    }
    public void click_pagar()
    {
       // driver.findElement(By.cssSelector(".button:nth-child(3)")).click();
        WebElement pagar=Util.driver.findElement(By.cssSelector(".button:nth-child(3)"));
        String actual = pagar.getAttribute("value");
        Util.assert_igual("PAGO DE TARJETAS", "Click en botón Pagar", actual, "Pagar", true, "N");
        pagar.click();
        Util.AvanzarPagina();
    }
    public void valida_pagoOK()
    {
        boolean mensaje= Util.driver.getPageSource().contains("Ha sido exitoso el pago de su tarjeta");
        System.out.println(mensaje);
        String actual;

        if (mensaje=true) actual="Pago exitoso";
        else
            actual="Fallido";
        Util.assert_contiene("PAGO DE TARJETAS", "Verificación de estado del pago", actual,"Pago exitoso", true,"N");
        System.out.println(mensaje);

    }


}
