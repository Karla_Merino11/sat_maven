package TestPages;

import Globales.CapturaPantalla;
import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public class Ingreso_OrdenPago_ImpAduanero {
    private Map<String, Object> vars;
    JavascriptExecutor js;
    CapturaPantalla print = new CapturaPantalla();
    public void click_link_pagos()
    {
        Util.driver.findElement(By.linkText("Pagos")).click();
    }

    public void click_link_carga()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(1);
        Util.waitForElementToBeClickable(By.id("306"));
        WebElement carga = Util.driver.findElement(By.id("306"));
        String actual = carga.getText();
        Util.assert_igual("SUBMENU", "Verificación de etiqueta", actual, "Carga/Transmision", false, "N");
        carga.click();
}
    public void clic_link_impaduaneros()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(2);
        //Util.driver.findElement(By.linkText("Pago IESS")).click();
        WebElement carga = Util.driver.findElement(By.linkText("Ingreso de Impuestos Aduaneros"));
        String actual = carga.getText();
        Util.assert_contiene("SUBMENU","Verificacion de etiqueta",actual,"Impuestos Aduaneros",false,"N");
        carga.click();
    }
    public void selec_cuenta(String num_cuenta)
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.name("cbo_ctaautpg"));
        WebElement cbo_cuenta = Util.driver.findElement(By.name("cbo_ctaautpg"));
        Select cuenta = new Select(cbo_cuenta) ;
        cuenta.selectByVisibleText(num_cuenta);
        Util.assert_contiene("ORDEN DE PAGO IMPUESTOS ADUANEROS", "Selecciona cuenta", num_cuenta, "", false, "N");

    }
    public void ingresa_referencia(String ref)
    {
        // Util.waitForElementToBeClickable(By.name("txt_referencia"));
        WebElement txt_referencia = Util.driver.findElement(By.name("txt_referencia"));
        txt_referencia.click();
        txt_referencia.sendKeys(ref);
        Reporte.agregarPaso("ORDEN DE PAGO IMPUESTOS ADUANEROS", "Ingreso referencia", ref, "", false, "N");
        Util.AvanzarPagina();
    }
    public void ingresa_ordensap(String orden)
    {
        WebElement ordensap= Util.driver.findElement(By.name("txt_ordensap"));
        ordensap.click();
        ordensap.sendKeys(orden);
        Reporte.agregarPaso("ORDEN DE PAGO IMPUESTOS ADUANEROS", "Ingreso orden sap", orden, "", false, "N");

    }
    public void clic_btningresar()
    {
        WebElement ingresarorden= Util.driver.findElement(By.name("btnBoton3"));
        String actual = ingresarorden.getAttribute("value");
        Util.assert_igual("ORDEN DE PAGO IMPUESTOS ADUANEROS", "Click en boton Ingresa datos", actual, "Ingresar datos", true, "N");
        ingresarorden.click();
        Util.AvanzarPagina();
    }
    public void ingresa_suministro(String suministro)
    {
        WebElement ruc_bene=Util.driver.findElement(By.name("txt_papeleta1"));
        ruc_bene.click();
        ruc_bene.sendKeys(suministro);
        Reporte.agregarPaso("ORDEN DE PAGO IMPUESTOS ADUANEROS","Ingresa Nro de Liquidacion",suministro,"",false,"N");
    }
    public void ingresa_nombene(String ref)
    {
        WebElement ruc_bene=Util.driver.findElement(By.name("txt_ref1"));
        ruc_bene.click();
        ruc_bene.sendKeys(ref);
        Reporte.agregarPaso("ORDEN DE PAGO IMPUESTOS ADUANEROS","Ingresa referencia",ref,"",false,"N");
        Util.RetrocederPagina();
    }
    public void consulta_liquidacion()
    {
        WebElement btn_consultar=Util.driver.findElement(By.name("btnBoton4"));
        String actual = btn_consultar.getAttribute("value");
        btn_consultar.click();
        Util.AvanzarPagina();
        Util.assert_igual("ORDEN DE PAGO IMPUESTOS ADUANEROS", "Click en botón Consultar", actual, "Consultar", true, "N");

    }
    public void genera_orden(String forma_pago,String cta)
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        String sat= Util.driver.getWindowHandle();
        System.out.println("pantalla principal sat " + sat);
        //Almacena el ID de la ventana original
        String originalWindow =Util.driver.getWindowHandle();
        //Comprueba que no existen otras ventanas abiertas previamente
        assert Util.driver.getWindowHandles().size() == 1;
        //Haz clic en el enlace el cual abre una nueva ventana
        Util.driver.findElement(By.name("btnBoton6")).click();
        //Espera a la nueva ventana o pestaña
        //Recorrelas hasta encontrar el controlador de la nueva ventana
        for (String windowHandle : Util.driver.getWindowHandles()) {
            if(!originalWindow.contentEquals(windowHandle)) {
                Util.driver.switchTo().window(windowHandle);
                break;
            }
        }
        //Espera a que la nueva ventana cargue su contenido

        WebElement pago = Util.driver.findElement(By.name("frm_pago"));
        Select i = new Select(pago);
        i.selectByVisibleText(forma_pago);

        Reporte.agregarPaso("ORDEN DE PAGO IMPUESTOS ADUANEROS", "Selecciona forma de pago", forma_pago, "", false, "N");

        WebElement cta_deb =Util.driver.findElement(By.name("cbo_cta_debito"));
        Select j = new Select(cta_deb);
        j.selectByVisibleText(cta);

        Reporte.agregarPaso("ORDEN DE PAGO IMPUESTOS ADUANEROS", "Selecciona Cta Debito", cta, "", true, "N");

        WebElement valor=Util.driver.findElement(By.name("txt_ApNC1"));
        valor.click();
        valor.sendKeys("\t");

        WebElement btn_generar=Util.driver.findElement(By.name("btnBoton5"));
        btn_generar.click();

        //*** volver a la pagina original
        //Cambia el controlador a la ventana o pestaña original
        Util.driver.switchTo().window(originalWindow);
        //Almacena el elemento web
        WebElement frame= Util.driver.findElement(By.id("oFrame"));
        //Cambia el foco al iframe
        Util.driver.switchTo().frame(frame);
        Util.AvanzarPagina();

    }
    public void vp_mensaje()
    {
        Util.driver.findElement(By.id("cuerpo")).click();
        Util.AvanzarPagina();

        boolean mensaje= Util.driver.getPageSource().contains("Verificado O.K.");
        if (mensaje)
            Reporte.agregarPaso("ORDEN DE PAGO IMPUESTOS ADUANEROS", "Verificacion de mensaje de confirmacion", "Orden ingresada exitosamente", "", true, "N");
        else
            Reporte.agregarPaso("ORDEN DE PAGO IMPUESTOS ADUANEROS", "Verificacion de mensaje de confirmacion", "ERROR", "", true, 1, "N");
        //como validar que la acción en la table ID=TABLE_CONSULTA_CARGA el resultado es Verificado O.K.
    }

}
