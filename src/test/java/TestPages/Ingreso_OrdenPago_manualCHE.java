package TestPages;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public class Ingreso_OrdenPago_manualCHE {

    public  String tipo_id_bene=null;
    public String cedula_benef = null;
    public String nombre_benef = null;
    public String concepto_benef = null;
    public String valor = null;

    @FindBy(name = "cbo_servicio")
    WebElement serviciosel=null;
    @FindBy(name="cbo_ctaautpg")
    WebElement cuentasel=null;
    @FindBy(name="txt_referencia")
    WebElement ingreferencia=null;
    @FindBy(name="btnBoton3")
    WebElement btnIngresar=null;
    @FindBy(name = "rdfrmpag")
    WebElement selecFormaPago=null;
    @FindBy(name="btnBoton4")
    WebElement btnGenerar=null;


    public Ingreso_OrdenPago_manualCHE() {
        PageFactory.initElements(Util.driver, this);
    }
    public void click_link_pagos()
    {
        Util.driver.findElement(By.linkText("Pagos")).click();
    }

    public void click_link_carga()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(1);
        Util.waitForElementToBeClickable(By.id("306"));
        WebElement carga = Util.driver.findElement(By.id("306"));
        String actual = carga.getText();
        Util.assert_igual("SUBMENU", "Verificación de etiqueta", actual, "Carga/Transmision", true, "N");
        carga.click();
    }

    public void click_ingresamanual()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(2);
        Util.waitForElementToBeClickable(By.linkText("Ingresar orden manualmente"));
        WebElement manual = Util.driver.findElement(By.linkText("Ingresar orden manualmente"));
        String actual = manual.getText();
        Util.assert_contiene("SUBMENU", "Verificación de etiqueta", actual, "Ingresar orden manualmente", true, "N");
        manual.click();
        Util.AvanzarPagina();
    }

    public void selecciono_servicio(String servicio)
    {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.name("cbo_servicio"));
        Select serv= new Select(serviciosel);
        serv.selectByVisibleText(servicio);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Servicio", servicio, "", false, "N");

    }
        public void selecciono_cuenta (String cuenta)
        {

            Util.waitForElementToBeClickable(By.name("cbo_ctaautpg"));
            Select cta= new Select(cuentasel);
            cta.selectByVisibleText(cuenta);
            Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Cuenta", cuenta, "", false, "N");

        }
    public void ingreso_referencia(String referencia)
    {
        ingreferencia.click();
        ingreferencia.sendKeys(referencia);
        Reporte.agregarPaso("ORDEN DE PAGO", "Ingreso referencia", referencia, "", false, "N");
    }

    public void click_boton_ingresa()
    {
        Util.waitForElementToBeClickable(By.name("btnBoton3"));
        String actual = btnIngresar.getAttribute("value");
        Util.assert_igual("ORDEN DE PAGO", "Click en botón Ingresar Datos", actual, "Ingresar datos", true, "N");
        btnIngresar.click();
    }

    public void selecciono_tipopago(String tipopago)
    {
        Util.waitForElementToBeClickable(By.name("rdfrmpag"));
        Select forma_pago= new Select(selecFormaPago);
        forma_pago.selectByVisibleText(tipopago);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Forma de Pago", tipopago, "", true, "N");
        Util.AvanzarPagina();
    }
    public void selec_tipo_idbene(int i)
    {
        WebElement tipo = Util.driver.findElement(By.name("chetipo"+i));
        Select sel_id =new Select(tipo);
        sel_id.selectByVisibleText(tipo_id_bene);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Tipo de identificacion de beneficiario"+i, tipo_id_bene, "", false, "N");
        tipo.sendKeys("\t");
    }

    public void ingreso_celula_benef(int i)
    {
        WebElement cobcedula = Util.driver.findElement(By.name("checedula"+i));
        cobcedula.click();
        cobcedula.sendKeys(cedula_benef);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Cédula de Beneficiario-detalle"+i, cedula_benef, "", false, "N");
        cobcedula.sendKeys("\t");
    }
    public void ingreso_nombre_benef(int i)
    {
        WebElement cobnombre = Util.driver.findElement(By.name("chenombre"+i));
        cobnombre.sendKeys(nombre_benef);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Nombre de Beneficiario-detalle"+i, nombre_benef, "", false, "N");
        cobnombre.sendKeys("\t");
    }
    public void ingreso_concepto(int i) {
        WebElement cobconcepto = Util.driver.findElement(By.name("checoncepto"+i));
        cobconcepto.sendKeys(concepto_benef);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Concepto-detalle"+i, concepto_benef, "", false, "N");
        cobconcepto.sendKeys("\t");
    }

    public void ingreso_valor(int i){
        WebElement cobvalor = Util.driver.findElement(By.name("chevalor"+i));
        cobvalor.click();
        cobvalor.sendKeys(valor);
        Reporte.agregarPaso("ORDEN DE PAGO", "Ingreso Valor-detalle"+i, valor, "", false, "N");
    }
    public void ingreso_datos_benef(int j)
    {

        for (int i=j;i<=j;i++)
        {
            selec_tipo_idbene(i);
            ingreso_celula_benef(i);
            ingreso_nombre_benef(i);
            ingreso_concepto(i);
            ingreso_valor(i);

        }
        Util.RetrocederPagina();
    }
    public void click_generar_orden()
    {
        //WebElement btnGenerar = Util.driver.findElement(By.name("btnBoton4"));
        String actual = btnGenerar.getAttribute("value");
        Util.assert_igual("ORDEN DE PAGO", "Click en botón Generar Orden", actual, "Generar orden", true, "N");
        btnGenerar.click();
    }

    public void vp_mensaje()
    {
        Util.driver.findElement(By.id("cuerpo")).click();
        Util.AvanzarPagina();

        boolean mensaje= Util.driver.getPageSource().contains("Verificado O.K.");
        if (mensaje)
            Reporte.agregarPaso("ORDEN DE PAGO", "Verificación de mensaje de confirmación", "Pago exitoso", "", true, "N");
        else
            Reporte.agregarPaso("ORDEN DE PAGO", "Verificación de mensaje de confirmación", "ERROR", "", true, 1, "N");
    }
    }


