package TestPages;// Generated by Selenium IDE
import Globales.CapturaPantalla;
import Globales.Reporte;
import Globales.Util;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.*;

public class PagodeTarjetasPropias {

    private Map<String, Object> vars;
    JavascriptExecutor js;
    CapturaPantalla print = new CapturaPantalla();

    public void click_link_24online()
    {
        Util.driver.findElement(By.linkText("24online")).click();
    }
    public void click_link_tarjetacredito()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(1);
        Util.waitForElementToBeClickable(By.id("1002"));
        Util.driver.findElement(By.id("1002")).click();
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(2);
        Util.driver.findElement(By.linkText("Pago de Tarjetas Propias")).click();


    }
    public void selec_empresa(String selempresa){
        //SELECCIONA UNA OPCION DE UNA LISTA
        Select empresa = new Select(Util.driver.findElement(By.name("cbo_empresa")));
        empresa.selectByVisibleText(selempresa);
        Util.driver.findElement(By.name("cbo_empresa")).click();
        Reporte.agregarPaso("PAGO DE TARJETAS","Verificacion de etiqueta",selempresa,"",false,"N");

    }
    public void selec_tarjeta(String seltarjeta){
        Select tarjeta= new Select(Util.driver.findElement(By.id("cmb_tarjeta")));
        tarjeta.selectByVisibleText(seltarjeta);
        // driver.findElement(By.name("cmb_tarjeta")).click();
        Util.AvanzarPagina();
        Reporte.agregarPaso("PAGO DE TARJETAS","Selecciona tarjeta",seltarjeta,"",false,"N");

    }

    public void selec_cuenta(String selcuenta){
        Select cuenta = new Select(Util.driver.findElement(By.name("cmb_cuentas")));
        cuenta.selectByVisibleText(selcuenta);
        Reporte.agregarPaso("PAGO DE TARJETAS","Selecciona cuenta debito",selcuenta,"",false,"N");

    }

    public void ingresa_monto(String selmonto){
        WebElement monto = Util.driver.findElement(By.id("txtmonto"));
        monto.click();
        monto.sendKeys(selmonto);
        Reporte.agregarPaso("PAGO DE TARJETAS","Ingreso de monto",selmonto,"",false,"N");

    }

    public void ingresa_concepto(String selconcepto){
        WebElement con = Util.driver.findElement(By.id("txtconcepto"));
        con.click();
        con.sendKeys(selconcepto);
        Reporte.agregarPaso("PAGO DE TARJETAS","Ingreso de concepto",selconcepto,"",false,"N");

    }
    public void click_continuar(){
        WebElement ingresa = Util.driver.findElement(By.id("idbtnContinuar"));
        String actual = ingresa.getAttribute("value");
        Util.assert_igual("PAGO DE TARJETAS", "Click en botón Continuar", actual, "Continuar", true, "N");
        ingresa.click();
    }

    public void ingresa_token(String seltoken)
    {
        Util.AvanzarPagina();
        WebElement token=Util.driver.findElement(By.id("txttoken"));
        token.click();
        token.sendKeys(seltoken);
        Reporte.agregarPaso("PAGO DE TARJETAS","Ingresa token",seltoken,"",false,"N");
    }

    public void click_pagar()
    {
        // driver.findElement(By.cssSelector(".button:nth-child(3)")).click();
        WebElement pagar=Util.driver.findElement(By.id("idbtnPagar"));
        String actual = pagar.getAttribute("value");
        Util.assert_igual("PAGO DE TARJETAS", "Click en botón Pagar", actual, "Pagar", true, "N");
        pagar.click();
        Util.AvanzarPagina();
    }

    public void valida_pagoOK()
    {
        Util.driver.findElement(By.id("cuerpo")).click();
        Util.RetrocederPagina();

        boolean mensaje= Util.driver.getPageSource().contains("Ha sido exitoso el pago de su tarjeta");
        System.out.println(mensaje);
        if (mensaje)
            Reporte.agregarPaso("PAGO DE TARJETAS", "Verificacion de mensaje de confirmacion", "Orden ingresada exitosamente", "", true, "N");
        else
            Reporte.agregarPaso("PAGO DE TARJETAS", "Verificacion de mensaje de confirmacion", "ERROR", "", true, 1, "N");
        //como validar que la acción en la table ID=TABLE_CONSULTA_CARGA el resultado es Verificado O.K.

    }

    }