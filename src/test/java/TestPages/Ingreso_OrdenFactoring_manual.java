package TestPages;

import Globales.CapturaPantalla;
import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public class Ingreso_OrdenFactoring_manual {

    private Map<String, Object> vars;
    JavascriptExecutor js;
    CapturaPantalla print = new CapturaPantalla();
    public String tipo = null;
    public String id = null;
    public String nombre = null;
    public String mod = null;
    public String gastos = null;
    public String valor = null;
    public String fac1 = null;
    public String fac2 = null;
    public String fac3 = null;
    public String dia = null;
    public String concepto = null;


    public void click_link_factoring()
    {
        Util.driver.findElement(By.linkText("Factoring Electronico")).click();
    }
    public void click_link_carga(){
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(1);
        Util.waitForElementToBeClickable(By.id("701"));
        WebElement carga = Util.driver.findElement(By.id("701"));
        String actual = carga.getText();
        Util.assert_igual("SUBMENU", "Verificación de etiqueta", actual, "Carga Transmision", false, "N");
        carga.click();
    }
    public void click_link_cargamanual(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(2);
        Util.driver.findElement(By.linkText("Ingreso manual de facturas")).click();
        Reporte.agregarPaso("SUBMENU","Clic Ingreso manual de facturas","Ingreso manual de facturas","",false,"N");
    }

    public void selec_servicio(String nom_servicio)
    {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.name("cbo_servicio"));
        WebElement cbo_servicio1 = Util.driver.findElement(By.name("cbo_servicio"));
        Select servicio = new Select(cbo_servicio1) ;
        servicio.selectByVisibleText(nom_servicio);
        Util.assert_contiene("ORDEN FACTORING", "Selecciona servicio", nom_servicio, "", false, "N");

    }

    public void selec_cuenta(String num_cuenta)
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.name("cbo_ctaautpg"));
        WebElement cbo_cuenta = Util.driver.findElement(By.name("cbo_ctaautpg"));
        Select cuenta = new Select(cbo_cuenta) ;
        cuenta.selectByVisibleText(num_cuenta);
        Util.assert_contiene("ORDEN FACTORING", "Selecciona cuenta", num_cuenta, "", false, "N");
    }
    public void ingresa_referencia(String ref)
    {
        WebElement txt_referencia = Util.driver.findElement(By.name("txt_referencia"));
        txt_referencia.click();
        txt_referencia.sendKeys(ref);
        Reporte.agregarPaso("ORDEN FACTORING", "Ingreso referencia", ref, "", false, "N");
        Util.AvanzarPagina();
    }
    public void clic_btningresar()
    {
        WebElement ingresarorden= Util.driver.findElement(By.name("btnBoton3"));
        String actual = ingresarorden.getAttribute("value");
        Util.assert_igual("ORDEN FACTORING", "Click en boton Generar Orden", actual, "Ingresar datos", true, "N");
        ingresarorden.click();
        Util.AvanzarPagina();
    }

    // Aqui se ingresa el detalle de la orden

    public void selec_tipoid(int i)
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.id("seltipoiden_"+i));
        WebElement tipoid = Util.driver.findElement(By.name("seltipoiden_"+i));
        Select id = new Select(tipoid) ;
        id.selectByVisibleText(tipo);
        Reporte.agregarPaso("ORDEN FACTORING","Selecciona tipo de identificacion - datalle"+i,tipo,"",false,"N");

    }
    public void ingresa_id(int i){
        WebElement ruc_bene=Util.driver.findElement(By.name("txtidentif_"+i));
        ruc_bene.click();
        ruc_bene.sendKeys(id);
        Reporte.agregarPaso("ORDEN FACTORING","Ingresa identificacion - datalle"+i,id,"",false,"N");
        ruc_bene.sendKeys("\t"); //comando para dar tab
    }
    public void ingresa_proveedor(int i){
        WebElement ruc_bene=Util.driver.findElement(By.name("txtprov_"+i));
        ruc_bene.click();
        ruc_bene.sendKeys(nombre);
        Reporte.agregarPaso("ORDEN FACTORING","Ingresa nombre del proveedor - datalle"+i,nombre,"",false,"N");
       // ruc_bene.sendKeys("\t"); //comando para dar tab
    }
    public void selec_modalidadpago(int i)
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.id("selformapago_1"));
        WebElement tipoid = Util.driver.findElement(By.name("selformapago_"+i));
        Select id = new Select(tipoid) ;
        id.selectByVisibleText(mod);
        Reporte.agregarPaso("ORDEN FACTORING","Selecciona modalidad de pago - datalle"+i,mod,"",false,"N");

    }

    public void selec_gastosfin(int i)
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.id("seltipodesembolso_"+i));
        WebElement tipoid = Util.driver.findElement(By.name("seltipodesembolso_"+i));
        Select id = new Select(tipoid) ;
        id.selectByVisibleText(gastos);
        Reporte.agregarPaso("ORDEN FACTORING","Selecciona gastos financieros - datalle"+i,gastos,"",false,"N");

    }
    public void ingresa_valor(int i){
        WebElement ruc_bene=Util.driver.findElement(By.id("txtmonto_"+i));
        ruc_bene.click();
        ruc_bene.sendKeys(valor);
        Reporte.agregarPaso("ORDEN FACTORING","Ingresa valor - datalle"+i,valor,"",false,"N");
        ruc_bene.sendKeys("\t"); //comando para dar tab
    }

    public void ingresa_fac1(int i){
        WebElement ruc_bene=Util.driver.findElement(By.id("txtlocalidad_"+i));
        ruc_bene.click();
        ruc_bene.sendKeys(fac1);
        Reporte.agregarPaso("ORDEN FACTORING","Ingresa numero de factura - datalle"+i,fac1,"",false,"N");
        ruc_bene.sendKeys("\t"); //comando para dar tab
    }
    public void ingresa_fac2(int i){

        WebElement fac=Util.driver.findElement(By.id("txtptovta_"+i));
        //fac.click();
        fac.sendKeys(fac2);
        Reporte.agregarPaso("ORDEN FACTORING","Ingresa numero de factura - datalle"+i,fac2,"",false,"N");
        fac.sendKeys("\t"); //comando para dar tab
    }
    public void ingresa_fac3(int i){

        WebElement fac=Util.driver.findElement(By.id("txtsecfactura_"+i));
       // ruc_bene.click();
        fac.sendKeys(fac3);
        Reporte.agregarPaso("ORDEN FACTORING","Ingresa numero de factura - datalle"+i,fac3,"",false,"N");
        fac.sendKeys("\t"); //comando para dar tab
    }
    public void ingresa_dia_ven(int i){
        WebElement dia_1=Util.driver.findElement(By.id("txt_fecvcto_dd_"+i));
        //dia.click();
        dia_1.sendKeys(dia);
        Reporte.agregarPaso("ORDEN FACTORING","Fecha de vencimiento - datalle"+i,dia,"",false,"N");
        dia_1.sendKeys("\t"); //comando para dar tab
    }

    public void ingresa_concepto(int i){
        WebElement ruc_bene=Util.driver.findElement(By.id("txt_concepto_"+i));
        ruc_bene.click();
        ruc_bene.sendKeys(concepto);
        Reporte.agregarPaso("ORDEN FACTORING","Ingresa concepto"+i,concepto,"",false,"N");
       // ruc_bene.sendKeys("\t"); //comando para dar tab
    }

    public void clic_geraorden()
    {
        Util.RetrocederPagina();
        WebElement ingresarorden= Util.driver.findElement(By.name("btnBoton4"));
        String actual = ingresarorden.getAttribute("value");
        Util.assert_igual("ORDEN FACTORING", "Click en boton Generar Orden", actual, "Generar orden", true, "N");
        ingresarorden.click();
        Util.AvanzarPagina();
    }
    public void vp_mensaje()
    {
        Util.driver.findElement(By.id("cuerpo")).click();
        Util.AvanzarPagina();

        boolean mensaje= Util.driver.getPageSource().contains("Verificado O.K.");
        if (mensaje)
            Reporte.agregarPaso("ORDEN FACTORING", "Verificacion de mensaje de confirmacion", "Orden ingresada exitosamente", "", true, "N");
        else
            Reporte.agregarPaso("ORDEN FACTORING", "Verificacion de mensaje de confirmacion", "ERROR", "", true, 1, "N");
        //como validar que la acción en la table ID=TABLE_CONSULTA_CARGA el resultado es Verificado O.K.
    }

    public void ingreso_datos_benef(int j)
    {
       // int j=2;
        for (int i=j;i<=j;i++)
        {

            selec_tipoid(i);
            ingresa_id(i);
            ingresa_proveedor(i);
            selec_modalidadpago(i);
            selec_gastosfin(i);
            ingresa_valor(i);
            ingresa_fac1(i);
            ingresa_fac2(i);
            ingresa_fac3(i);
            ingresa_dia_ven(i);
            ingresa_concepto(i);
         //   j++;
        }
        Util.RetrocederPagina();
    }


}
