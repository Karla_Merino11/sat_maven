package TestPages;

import Globales.CapturaPantalla;
import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public class Ingreso_OrdenPago_manualCOB {
    //private WebUtil.driver Util.driver;
    private Map<String, Object> vars;
    JavascriptExecutor js;

    public  String tipo_id_bene=null;
    public String cedula_benef = null;
    public String nombre_benef = null;
    public String concepto_benef = null;
    public String tipo_cuenta_benef = null;
    public String num_cuenta_benef = null;
    public String valor = null;
    int banco=2;

    public void click_link_pagos()
    {
        Util.driver.findElement(By.linkText("Pagos")).click();
    }

    public void click_link_carga()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(1);
        Util.waitForElementToBeClickable(By.id("306"));
        WebElement carga = Util.driver.findElement(By.id("306"));
        String actual = carga.getText();
        Util.assert_igual("SUBMENU", "Verificación de etiqueta", actual, "Carga/Transmision", true, "N");
        carga.click();
    }

    public void click_ingresamanual()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(2);
        Util.waitForElementToBeClickable(By.linkText("Ingresar orden manualmente"));
        WebElement manual = Util.driver.findElement(By.linkText("Ingresar orden manualmente"));
        String actual = manual.getText();
        Util.assert_contiene("SUBMENU", "Verificación de etiqueta", actual, "Ingresar orden manualmente", true, "N");
        manual.click();
        Util.AvanzarPagina();
    }

    public void selecciono_servicio(String servicio)
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.waitForElementToBeClickable(By.name("cbo_servicio"));
        WebElement cbo_servicio = Util.driver.findElement(By.name("cbo_servicio"));
        Select serviciosel= new Select(cbo_servicio);
        serviciosel.selectByVisibleText(servicio);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Servicio", servicio, "", true, "N");
    }

    public void selecciono_cuenta(String cuenta)
    {
        Util.waitForElementToBeClickable(By.name("cbo_ctaautpg"));
        WebElement cbo_ctaautpg = Util.driver.findElement(By.name("cbo_ctaautpg"));
        Select cuentasel= new Select(cbo_ctaautpg);
        cuentasel.selectByVisibleText(cuenta);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Cuenta", cuenta, "", true, "N");
    }

    public void ingreso_referencia(String referencia)
    {
        Util.waitForElementToBeClickable(By.name("txt_referencia"));
        WebElement txt_referencia = Util.driver.findElement(By.name("txt_referencia"));
        txt_referencia.click();
        txt_referencia.sendKeys(referencia);
        Reporte.agregarPaso("ORDEN DE PAGO", "Ingreso referencia", referencia, "", true, "N");
    }

    public void click_boton_ingresa()
    {
        Util.waitForElementToBeClickable(By.name("btnBoton3"));
        WebElement btnIngresar = Util.driver.findElement(By.name("btnBoton3"));
        String actual = btnIngresar.getAttribute("value");
        Util.assert_igual("ORDEN DE PAGO", "Click en botón Ingresar Datos", actual, "Ingresar datos", false, "N");
        btnIngresar.click();
    }

    public void selecciono_tipopago(String tipopago)
    {
        Util.waitForElementToBeClickable(By.name("rdfrmpag"));
        WebElement cbo_tipopago = Util.driver.findElement(By.name("rdfrmpag"));
        Select forma_pago= new Select(cbo_tipopago);
        forma_pago.selectByVisibleText(tipopago);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Forma de Pago", tipopago, "", true, "N");
        Util.AvanzarPagina();
    }
    public void selec_tipo_idbene(int i)
    {
        WebElement tipo = Util.driver.findElement(By.name("cobtipo"+i));
        Select sel_id =new Select(tipo);
        sel_id.selectByVisibleText(tipo_id_bene);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Tipo de identificacion de beneficiario"+i, tipo_id_bene, "", false, "N");
        tipo.sendKeys("\t");
    }
    public void ingreso_celula_benef(int i)
    {
        WebElement cobcedula = Util.driver.findElement(By.name("cobcedula"+i));
        cobcedula.click();
        cobcedula.sendKeys(cedula_benef);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Cédula de Beneficiario-detalle"+i, cedula_benef, "", false, "N");
        cobcedula.sendKeys("\t");
    }

    public void ingreso_nombre_benef(int i)
    {
        WebElement cobnombre = Util.driver.findElement(By.name("cobnombre"+i));
        //cobnombre.click();
        cobnombre.sendKeys(nombre_benef);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Nombre de Beneficiario-detalle"+i, nombre_benef, "", false, "N");
        cobnombre.sendKeys("\t");
    }

    public void ingreso_concepto(int i) {
        WebElement cobconcepto = Util.driver.findElement(By.name("cobconcepto"+i));
        //cobconcepto.click();
        cobconcepto.sendKeys(concepto_benef);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Concepto-detalle"+i, concepto_benef, "", false, "N");
        cobconcepto.sendKeys("\t");
    }

    public void ingreso_banco_benef(int banco){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        WebElement cobbanco = Util.driver.findElement(By.cssSelector("tr:nth-child("+banco+") .custom-combobox-input"));
        cobbanco.sendKeys("BANCO PICHINCHA");
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Banco", "BANCO PICHINCHA", "", false, "N");
       // cobbanco.sendKeys("\t");
    }

    public void ingreso_tipocta_benef(int i){
        WebElement cobtipcuenta = Util.driver.findElement(By.name("cobtipcuenta"+i));
        cobtipcuenta.sendKeys(tipo_cuenta_benef);
        Reporte.agregarPaso("ORDEN DE PAGO", "Selecciono Tipo de Cuenta-detalle"+i, tipo_cuenta_benef, "", false, "N");
    }

    public void ingreso_numcta_benef(int i){
        WebElement cobnumcuenta = Util.driver.findElement(By.name("cobnumcuenta"+i));
        cobnumcuenta.click();
        cobnumcuenta.sendKeys(num_cuenta_benef);
        Reporte.agregarPaso("ORDEN DE PAGO", "Ingreso Número de Cuenta-detalle"+i, num_cuenta_benef, "", false, "N");
        cobnumcuenta.sendKeys("\t");
    }

    public void ingreso_valor(int i){
        WebElement cobvalor = Util.driver.findElement(By.name("cobvalor"+i));
        cobvalor.click();
        cobvalor.sendKeys(valor);
        Reporte.agregarPaso("ORDEN DE PAGO", "Ingreso Valor-detalle"+i, valor, "", false, "N");

    }

    public void ingreso_datos_benef(int j)
    {

        for (int i=j;i<=j;i++)
        {
            selec_tipo_idbene(i);
            ingreso_celula_benef(i);
            ingreso_nombre_benef(i);
            ingreso_concepto(i);
            ingreso_banco_benef(banco);
            ingreso_tipocta_benef(i);
            ingreso_numcta_benef(i);
            ingreso_valor(i);
            banco ++;
        }
        Util.RetrocederPagina();
    }

    public void click_generar_orden()
    {
        WebElement btnGenerar = Util.driver.findElement(By.name("btnBoton4"));
        String actual = btnGenerar.getAttribute("value");
        Util.assert_igual("ORDEN DE PAGO", "Click en botón Generar Orden", actual, "Generar orden", true, "N");
        btnGenerar.click();
    }

    public void vp_mensaje()
    {
        Util.driver.findElement(By.id("cuerpo")).click();
        Util.AvanzarPagina();

        boolean mensaje= Util.driver.getPageSource().contains("Verificado O.K.");
        if (mensaje)
            Reporte.agregarPaso("ORDEN DE PAGO", "Verificación de mensaje de confirmación", "Pago exitoso", "", true, "N");
        else
            Reporte.agregarPaso("ORDEN DE PAGO", "Verificación de mensaje de confirmación", "ERROR", "", true, 1, "N");
        //como validar que la acción en la table ID=TABLE_CONSULTA_CARGA el resultado es Verificado O.K.
    }
}
