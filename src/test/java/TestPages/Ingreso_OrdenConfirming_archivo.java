package TestPages;
import Globales.CapturaPantalla;
import Globales.Util;
import Globales.Reporte;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import java.util.Map;
public class Ingreso_OrdenConfirming_archivo {

    private Map<String, Object> vars;
    JavascriptExecutor js;
    CapturaPantalla print = new CapturaPantalla();

    public void MenuConfirming()
    {
        Util.driver.findElement(By.linkText("Confirming")).click();
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(1);
    }
    public void SubmenuId()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(1);
        Util.waitForElementToBeClickable(By.id("901"));
        WebElement carga = Util.driver.findElement(By.id("901"));

        String actual = carga.getText();
        Util.assert_igual("SUBMENU", "Verificacion de etiqueta", actual,actual, true, "N");
        carga.click();
    }

    public void OpcionCarga()
    {
        Util.driver.switchTo().defaultContent();
        Util.driver.switchTo().frame(2);
        Util.waitForElementToBeClickable(By.linkText("Ingresar Archivo"));
        WebElement ingresar = Util.driver.findElement(By.linkText("Ingresar Archivo"));
        String actual = ingresar.getText();
        Util.assert_contiene("SUBMENU","Verificacion de etiqueta",actual,"Ingresar Archivo",true,"N");
        ingresar.click();
        Util.AvanzarPagina();
    }


    public void NombreServicio(String nom_servicio)
    {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        Util.waitForElementToBeClickable(By.name("cbo_servicio"));
        WebElement cbo_servicio1 = Util.driver.findElement(By.name("cbo_servicio"));
        Select servicio = new Select(cbo_servicio1) ;
        servicio.selectByVisibleText(nom_servicio);
        Util.assert_contiene("GENERA ORDEN POR CARGA DE ARCHIVO CONFIRMING", "Selecciona servicio", nom_servicio, "", false, "N");

    }
    public void NumeroCuenta(String num_cuenta)
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        Util.waitForElementToBeClickable(By.name("cbo_ctaautpg"));
        WebElement cbo_cuenta = Util.driver.findElement(By.name("cbo_ctaautpg"));
        Select cuenta = new Select(cbo_cuenta) ;
        cuenta.selectByVisibleText(num_cuenta);
        Util.assert_contiene("GENERA ORDEN POR CARGA DE ARCHIVO CONFIRMING", "Selecciona cuenta", num_cuenta, "", false, "N");
    }

    public void BuscaRuta_CargaArchivo(String carpeta,String nom_archivo) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        String sat= Util.driver.getWindowHandle();
        System.out.println("pantalla principal sat " + sat);
        //Almacena el ID de la ventana original
        String originalWindow =Util.driver.getWindowHandle();
        //Comprueba que no existen otras ventanas abiertas previamente
        assert Util.driver.getWindowHandles().size() == 1;
        //Haz clic en el enlace el cual abre una nueva ventana

        Util.driver.findElement(By.name("btnBoton2")).click();
        //Espera a la nueva ventana o pesta�a
        //Recorrelas hasta encontrar el controlador de la nueva ventana
        for (String windowHandle : Util.driver.getWindowHandles()) {
            if(!originalWindow.contentEquals(windowHandle)) {
                Util.driver.switchTo().window(windowHandle);
                break;
            }
        }
        //Espera a que la nueva ventana cargue su contenido

        WebElement uploadelement = Util.driver.findElement(By.name("txt_file"));
        uploadelement.sendKeys("C:\\Users\\kmerinob\\Documents\\Lecho de Testeo\\ARCHIVOS\\"+carpeta+"\\"+nom_archivo);

        System.out.println("obtiene ruta");
        Util.driver.findElement(By.id("btn_cargar")).click();
        Util.driver.findElement(By.id("button1")).click();
        //*** volver a la pagina original
        //Cambia el controlador a la ventana o pesta�a original
        Util.driver.switchTo().window(originalWindow);
        //Almacena el elemento web
        WebElement frame= Util.driver.findElement(By.id("oFrame"));

        //Cambia el foco al iframe
        Util.driver.switchTo().frame(frame);
        Reporte.agregarPaso("GENERA ORDEN POR CARGA DE ARCHIVO CONFIRMING", "Busca la ruta y Carga el archivo",nom_archivo  , "", false, "N");

    }

    public void Ingresa_Referencia(String ref)
    {
        WebElement txt_referencia = Util.driver.findElement(By.name("txt_referencia"));
        txt_referencia.click();
        txt_referencia.sendKeys(ref);
        Reporte.agregarPaso("GENERA ORDEN POR CARGA DE ARCHIVO CONFIRMING", "Ingresa la referencia", ref, "", false, "N");
        Util.AvanzarPagina();
    }

    public void Procesar_Archivo()
    {
        WebElement btnGenerar = Util.driver.findElement(By.name("btnBoton3"));
        String actual = btnGenerar.getAttribute("value");
        Util.assert_igual("GENERA ORDEN POR CARGA DE ARCHIVO CONFIRMING", "Click en boton Procesar", actual, "Procesar", false, "N");
        btnGenerar.click();
        Util.AvanzarPagina();
    }

    public void VerificaMensaje()
    {
        Util.driver.findElement(By.id("cuerpo")).click();
        Util.AvanzarPagina();

        boolean mensaje= Util.driver.getPageSource().contains("Ver errores");

        if  (mensaje)
            Reporte.agregarPaso("GENERA ORDEN POR CARGA DE ARCHIVO CONFIRMING", "Verificacion de mensaje de confirmacion", "ERROR", "", true, 1, "N");
        else
            Reporte.agregarPaso("GENERA ORDEN POR CARGA DE ARCHIVO CONFIRMING", "Verificacion de mensaje de confirmacion", "Archivo verificado", "", true, "N");

    }

}
