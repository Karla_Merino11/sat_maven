package RunScripts;

import Globales.LeeExcel;
import Globales.Reporte;
import Globales.Util;
import TestPages.Ingreso_OrdenPagos_archivo;
import TestPages.LoginSAT;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import java.io.IOException;

public class Run_OrdenPagos_Archivo {

    @Before
    public void iniciar_Chrome() {
        Util.Inicio("OrdenPago_Archivo","1");
        Reporte.setEntorno("Ambiente: Preproduccion" + "</b><br>" + "Navegador: Chrome; " + " Version: 88.0.4324.190 (Official Build) (64-bit)"  );
    }

    @Test
    public void Genera_ordenPagosRolesvarios() throws IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de Roles exitosa - Por carga de archivo");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("2");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\004ordenpagoSATarchivo.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 3);
        String archivo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 4);
        System.out.println(archivo);
        Ingreso_OrdenPagos_archivo orden = new Ingreso_OrdenPagos_archivo();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.clicl_link_ingresar();
        orden.selec_empresa(empresa);
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.carga_archivo(archivo);
        orden.clic_generaorden();
        orden.vp_mensaje();
        orden.clic_transmitir();


    }

    @Test
    public void Genera_ordenPagosProveedoresvarios() throws IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de Pago a Proveedores exitosa - Por carga de archivo");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("2");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\004ordenpagoSATarchivo.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 1);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 2);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 3);
        String archivo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 4);
        System.out.println(archivo);
        Ingreso_OrdenPagos_archivo orden = new Ingreso_OrdenPagos_archivo();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.clicl_link_ingresar();
        orden.selec_empresa(empresa);
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.carga_archivo(archivo);
        orden.clic_generaorden();
        orden.vp_mensaje();
        orden.clic_transmitir();


    }

    @Test
    public void Genera_ordenPagosProveedoresvariosCondesglose() throws IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de Pago a Proveedores con desglose exitosa - Por carga de archivo");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("2");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\004ordenpagoSATarchivo.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 1);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 2);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 3);
        String archivo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 4);
        System.out.println(archivo);
        Ingreso_OrdenPagos_archivo orden = new Ingreso_OrdenPagos_archivo();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.clicl_link_ingresar();
        orden.selec_empresa(empresa);
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.carga_archivo(archivo);
        orden.clic_generaorden();
        orden.vp_mensaje();
        orden.clic_transmitir();


    }

    @Test
    public void Genera_ordenPagosTerceros() throws IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden Pago a Terceros exitosa - Por carga de archivo");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("2");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\004ordenpagoSATarchivo.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 1);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 2);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 3);
        String archivo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 4);
        System.out.println(archivo);
        Ingreso_OrdenPagos_archivo orden = new Ingreso_OrdenPagos_archivo();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.clicl_link_ingresar();
        orden.selec_empresa(empresa);
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.carga_archivo(archivo);
        orden.clic_generaorden();
        orden.vp_mensaje();
        orden.clic_transmitir();


    }

    @Test
    public void Genera_ordenPagosProveedoresvarios_prepo() throws IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de Pago a Proveedores exitosa - Por carga de archivo");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("17");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\004ordenpagoSATarchivo.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 1);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 2);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 3);
        String archivo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 4);
        System.out.println(archivo);
        Ingreso_OrdenPagos_archivo orden = new Ingreso_OrdenPagos_archivo();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.clicl_link_ingresar();
        orden.selec_empresa(empresa);
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.carga_archivo(archivo);
        orden.clic_generaorden();
        orden.vp_mensaje();
        orden.clic_transmitir();


    }

    @Test
    public void Genera_ordenPagosProveedoresvarios_DESA1() throws IOException {
        //EMPRESA AGRIPAC
        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de Pago a Proveedores exitosa - Por carga de archivo");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("2");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\004ordenpagoSATarchivo.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 7, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 7, 1);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 7, 2);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 7, 3);
        String archivo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 7, 4);
        System.out.println(archivo);
        Ingreso_OrdenPagos_archivo orden = new Ingreso_OrdenPagos_archivo();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.clicl_link_ingresar();
        orden.selec_empresa(empresa);
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.carga_archivo(archivo);
        orden.clic_generaorden();
        orden.vp_mensaje();
        orden.clic_transmitir();


    }

    @Test
    public void Genera_ordenPagosTerceros_DESA1() throws IOException {
        //PAGO DE ADQUIERENCIASA ESTABLECIMIENTO JUNTA DE BENEFICIENCIA
        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de Pago a Terceros exitosa - Por carga de archivo");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("2");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\004ordenpagoSATarchivo.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 9, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 9, 1);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 9, 2);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 9, 3);
        String archivo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 9, 4);
        System.out.println(archivo);
        Ingreso_OrdenPagos_archivo orden = new Ingreso_OrdenPagos_archivo();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.clicl_link_ingresar();
        System.out.println(empresa);
        orden.selec_empresa(empresa);
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.carga_archivo(archivo);
        orden.clic_generaorden();
        orden.vp_mensaje();
        orden.clic_transmitir();


    }

    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }
}
