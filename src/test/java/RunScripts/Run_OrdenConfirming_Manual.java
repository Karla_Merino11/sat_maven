package RunScripts;

import Globales.LeeExcel;
import Globales.Reporte;
import Globales.Util;
import TestPages.Ingreso_OrdenConfirming_manual;
import TestPages.LoginSAT;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;


public class Run_OrdenConfirming_Manual {

    @Before
    public void iniciar_Chrome() {
        Util.Inicio("OrdenConfirming","1");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Navegador: Chrome; " + " Version: 88.0.4324.190 (Official Build) (64-bit)"  );
    }
    //*************PERFIL EMPRESA*********
    @Test
    public void Genera_ordenConfirming_Undetalle_AdelantoTotal() throws IOException {
        Reporte.setNombreReporte("Generacion de Orden Confirming Manual Un detalle- Adelanto total");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("9");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\010ordenconfirming.xlsx";
        //
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);
        String tipo_id = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 3);
        String identificacion = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 4);
        String prov = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 5);
        String mod_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 6);
        String gastos_fin = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 7);
        String opera_fin = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 8);
        String monto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 9);
        String factura1 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 10);
        String factura2 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 11);
        String factura3 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 12);
        String dia_ven = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 13);
        String mes_ven = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 14);
        String concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 16);

        Ingreso_OrdenConfirming_manual orden=new Ingreso_OrdenConfirming_manual();
        orden.click_link_confirming();
        orden.click_link_carga();
        orden.click_link_cargamanual();
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();
        //Se asigna datos de beneficiarios, actualizar i a la cantidad de detalles a leer de datapool
        for (int i=1;i<=1 ;i++) {
            orden.tipo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 3);
            orden.id = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 4);
            orden.nombre = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 5);
            orden.mod = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 6);
            orden.gastos = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 7);
            orden.opera = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 8);
            orden.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 9);
            orden.fac1 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 10);
            orden.fac2 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 11);
            orden.fac3 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 12);
            orden.dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 13);
            orden.mes_ven= excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 14);
            System.out.println(mes_ven);
            orden.concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 16);
            orden.ingreso_datos_benef(i);
        }
        orden.clic_geraorden();
        orden.vp_mensaje();
    }

    @Test
    public void Genera_ordenConfirming_Dosdetalle_PerfilEmpresa() throws IOException {
        Reporte.setNombreReporte("Generacion de Orden Factoring Manual Dos detalle-Adelanto total/linea");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("9");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\010ordenconfirming.xlsx";
        //
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);

        Ingreso_OrdenConfirming_manual orden=new Ingreso_OrdenConfirming_manual();
        orden.click_link_confirming();
        orden.click_link_carga();
        orden.click_link_cargamanual();
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();

        //Se asigna datos de beneficiarios
        for (int i=1;i<=2 ;i++) {
            orden.tipo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 3);
            orden.id = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 4);
            orden.nombre = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 5);
            orden.mod = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 6);
            orden.gastos = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 7);
            orden.opera = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 8);
            orden.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 9);
            orden.fac1 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 10);
            orden.fac2 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 11);
            orden.fac3 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 12);
            orden.dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 13);
            orden.concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 16);
            orden.ingreso_datos_benef(i);
        }

        orden.clic_geraorden();
        orden.vp_mensaje();
    }

    @Test
    public void Genera_ordenConfirming_Undetalle_AdelantoLinea() throws IOException {
        Reporte.setNombreReporte("Generacion de Orden Confirming Manual Un detalle- Adelanto en linea");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("9");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\010ordenconfirming.xlsx";
        //
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 2);
        String tipo_id = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 3);
        String identificacion = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 4);
        String prov = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 5);
        String mod_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 6);
        String gastos_fin = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 7);
        String opera_fin = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 8);
        String monto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 9);
        String factura1 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 10);
        String factura2 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 11);
        String factura3 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 12);
        String dia_ven = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 13);
        String concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 16);

        Ingreso_OrdenConfirming_manual orden=new Ingreso_OrdenConfirming_manual();
        orden.click_link_confirming();
        orden.click_link_carga();
        orden.click_link_cargamanual();
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();
        //Se asigna datos de beneficiarios, actualizar i a la cantidad de detalles a leer de datapool
        for (int i=1;i<=1 ;i++) {
            orden.tipo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 3);
            orden.id = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 4);
            orden.nombre = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 5);
            orden.mod = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 6);
            orden.gastos = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 7);
            orden.opera = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 8);
            orden.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 9);
            orden.fac1 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 10);
            orden.fac2 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 11);
            orden.fac3 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 12);
            orden.dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 13);
            orden.concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 16);
            orden.ingreso_datos_benef(i);
        }
        orden.clic_geraorden();
        orden.vp_mensaje();
    }
    //***************PERFIL PROVEEDOR***********
    @Test
    public void Genera_ordenConfirming_Dosdetalle_PerfilProveedor() throws IOException {
        Reporte.setNombreReporte("Generacion de Orden Confirming Manual Perfil Proveedor Dos detalle- Adelanto total/linea");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("15");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\010ordenconfirming.xlsx";
        //
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);
        String tipo_id = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 3);
        String identificacion = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 4);
        String prov = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 5);
        String mod_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 6);
        String gastos_fin = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 7);
        String opera_fin = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 8);
        String monto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 9);
        String factura1 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 10);
        String factura2 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 11);
        String factura3 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 12);
        String dia_ven = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 13);
        String mes_ven = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 14);
        String concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 16);
        String empresa=excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 17);


        Ingreso_OrdenConfirming_manual orden=new Ingreso_OrdenConfirming_manual();

        orden.click_link_confirming();
        orden.click_link_carga();
        orden.click_link_cargamanual();
        //Aqui se selecciona el perfil con el que se ingresa la orden
        orden.checkperfil();
        orden.selempresa(empresa);

        orden.selec_servicio(servicio);
       // orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();
        //Se asigna datos de beneficiarios, actualizar i a la cantidad de detalles a leer de datapool
        for (int i=1;i<=2 ;i++) {
            orden.tipo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 3);
            orden.id = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 4);
            orden.nombre = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 5);
            orden.mod = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 6);
            orden.gastos = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 7);
            orden.opera = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 8);
            orden.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 9);
            orden.fac1 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 10);
            orden.fac2 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 11);
            orden.fac3 = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 12);
            orden.dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 13);
            orden.mes_ven= excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 14);
            System.out.println(mes_ven);
            orden.concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", i, 16);
            orden.ingreso_datos_benef(i);
        }
        orden.clic_geraorden();
        orden.vp_mensaje();
    }
    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }
}
