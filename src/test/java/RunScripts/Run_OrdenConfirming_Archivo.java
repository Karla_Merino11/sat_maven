package RunScripts;
import Globales.Reporte;
import Globales.Util;
import TestPages.*;
import Globales.LeeExcel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import java.io.IOException;

public class Run_OrdenConfirming_Archivo {

    WebDriver driver;

    @Before
    public void iniciar_Chrome() {
        Util.Inicio("OrdenCargaConfirming","1");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Navegador: Chrome; " + " Version: 88.0.4324.190 (Official Build) (64-bit)"  );
    }

    //primera Fila en el Archivo de Excel de Datapool
    @Test
    public void Carga_GeneraOrdenConfirming() throws IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Genera Orden por Carga de archivo en Confirming - Exitosa");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("9");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\015GeneraOrdenCargaConfirming.xlsx";

        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String perf = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 3);
        String rutaArchivo=excel.getCellvalue(filepath_pgtarjeta,"Hoja1", 1,4);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 5);
        String archivo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 6);
        //  String archivoItems = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 6);


        System.out.println(archivo);
        System.out.println(rutaArchivo);
        Ingreso_OrdenConfirming_archivo Orden = new Ingreso_OrdenConfirming_archivo();
        Orden.MenuConfirming();
        Orden.SubmenuId();
        Orden.OpcionCarga();
        Orden.NombreServicio(servicio);
        Orden.NumeroCuenta(cuenta);
        Orden.BuscaRuta_CargaArchivo(rutaArchivo,archivo);
        Orden.Ingresa_Referencia(referencia);
        Orden.Procesar_Archivo();
        Orden.VerificaMensaje();

    }


    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }

}
