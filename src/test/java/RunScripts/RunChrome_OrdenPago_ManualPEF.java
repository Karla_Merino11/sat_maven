package RunScripts;

import Globales.LeeExcel;
import Globales.Reporte;
import Globales.Util;
import TestPages.Ingreso_OrdenPago_manualCHE;
import TestPages.Ingreso_OrdenPago_manualPEF;
import TestPages.LoginSAT;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class RunChrome_OrdenPago_ManualPEF {
    int fila=0;
    @Before
    public void iniciar_Chrome() {
        Util.Inicio("OrdenPagoPEF","1");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Navegador: Chrome; " + " Version: 88.0.4324.190 (Official Build) (64-bit)"  );
    }
    @Test
    public void Genera_ordenProveedoresExitosaPEF() throws IOException {
        Reporte.setNombreReporte("Generacion de Orden de Pago a Proveedores exitosa");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("5");
        //Indicar que fila del datapol va a leer
        fila=2;
        // Pool de datos ingreso de orden
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\016ordenpagoSAT_PEF.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 2);
        String tipo_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 3);


        Ingreso_OrdenPago_manualPEF orden_che = new Ingreso_OrdenPago_manualPEF();
        orden_che.click_link_pagos();
        orden_che.click_link_carga();
        orden_che.click_ingresamanual();
        orden_che.selecciono_servicio(servicio);
        orden_che.selecciono_cuenta(cuenta);
        orden_che.ingreso_referencia(referencia);
        orden_che.click_boton_ingresa();
        orden_che.selecciono_tipopago(tipo_pago);

        // ingreso de detalle de ordenes-los lee de la hoja2 del datapool
        for (int i=1;i<=5 ;i++) {
            orden_che.tipo_id_bene = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 0);
            orden_che.cedula_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 1);
            orden_che.nombre_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 2);
            orden_che.concepto_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 3);
            orden_che.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 4);
            orden_che.ingreso_datos_benef(i);

        }
        orden_che.click_generar_orden();
        orden_che.vp_mensaje();

    }
    @Test
    public void Genera_ordenTercerosExitosaPEF() throws IOException {
        Reporte.setNombreReporte("Generacion de Orden de Pago a Terceros exitosa");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("4");
        //Indicar que fila del datapol va a leer
        fila=3;
        // Pool de datos ingreso de orden
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\016ordenpagoSAT_PEF.xlsx";
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 0);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 1);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 2);
        String tipo_pago = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", fila, 3);


        Ingreso_OrdenPago_manualPEF orden_che = new Ingreso_OrdenPago_manualPEF();
        orden_che.click_link_pagos();
        orden_che.click_link_carga();
        orden_che.click_ingresamanual();
        orden_che.selecciono_servicio(servicio);
        orden_che.selecciono_cuenta(cuenta);
        orden_che.ingreso_referencia(referencia);
        orden_che.click_boton_ingresa();
        orden_che.selecciono_tipopago(tipo_pago);

        // ingreso de detalle de ordenes-los lee de la hoja2 del datapool
        for (int i=1;i<=5 ;i++) {
            orden_che.tipo_id_bene = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 0);
            orden_che.cedula_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 1);
            orden_che.nombre_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 2);
            orden_che.concepto_benef = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 3);
            orden_che.valor = excel.getCellvalue(filepath_pgtarjeta, "Hoja2", i, 4);
            orden_che.ingreso_datos_benef(i);

        }
        orden_che.click_generar_orden();
        orden_che.vp_mensaje();

    }
    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }

}
