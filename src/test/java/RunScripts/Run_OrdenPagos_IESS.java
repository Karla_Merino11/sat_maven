package RunScripts;

import Globales.LeeExcel;
import Globales.Reporte;
import Globales.Util;
import TestPages.Ingreso_OrdenPago_IESS;
import TestPages.LoginSAT;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class Run_OrdenPagos_IESS {
    @Before
    public void iniciar_Chrome() {
        Util.Inicio("OrdenPago_IEES","2");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Navegador: Chrome; " + " Version: 88.0.4324.190 (Official Build) (64-bit)"  );
    }
    @Test
    public void Genera_ordenPagosIEES_CTACTE() throws IOException {

        //Ingreso de orden con perfil empresa
        Reporte.setNombreReporte("Generacion de Orden de Pago IESS - CUENTA CORRIENTE, consulta de comprobante por sucursal");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("6");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\007ordenpagoIEES.xlsx";
        //String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String mes = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);
        String anio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 3);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 4);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 5);
        String ruc = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 6);
        String sucursal = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 7);

        Ingreso_OrdenPago_IESS orden = new Ingreso_OrdenPago_IESS();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.click_link_pagoiess();
        orden.selec_servicio(servicio);
       // orden.ingresa_diapago(dia);
        //orden.ingresa_mespago(mes);
        //orden.ingresa_aniopago(anio);
        System.out.println(cuenta);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();
        orden.ingresaruc(ruc);
        orden.ingresarsucursal(sucursal);
        orden.consultodeuda();
        orden.marcodeuda();
        orden.geraorden();
        orden.vp_mensaje();

    }
    @Test
    public void Genera_ordenPagosIEES_CTAAHO() throws IOException {
        //Ingreso de orden con perfil empresa
        Reporte.setNombreReporte("Generacion de Orden de Pago IESS - CUENTA AHORRO , consulta de comprobante por sucursal");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("6");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\007ordenpagoIEES.xlsx";
        //String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 0);
        String dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 1);
        String mes = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 2);
        String anio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 3);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 4);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 5);
        String ruc = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 6);
        String sucursal = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 2, 7);

        Ingreso_OrdenPago_IESS orden = new Ingreso_OrdenPago_IESS();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.click_link_pagoiess();
        orden.selec_servicio(servicio);
        // orden.ingresa_diapago(dia);
        //orden.ingresa_mespago(mes);
        //orden.ingresa_aniopago(anio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();
        orden.ingresaruc(ruc);
        orden.ingresarsucursal(sucursal);
        orden.consultodeuda();
        orden.marcodeuda();
        orden.geraorden();
        orden.vp_mensaje();

    }
    @Test
    public void Genera_ordenPagosIEES_ConceptoSucursalCTAAHO() throws IOException {

        //Ingreso de orden con perfil empresa
        Reporte.setNombreReporte("Generacion de Orden de Pago IESS - Con CTA AHO, consulta de comprobante por concepto y sucursal");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("6");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\007ordenpagoIEES.xlsx";
        //String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 0);
        String dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 1);
        String mes = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 2);
        String anio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 3);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 4);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 5);
        String ruc = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 6);
        String sucursal = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 7);
        String tipo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 8);
        String concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 3, 9);

        Ingreso_OrdenPago_IESS orden = new Ingreso_OrdenPago_IESS();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.click_link_pagoiess();
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();
        orden.ingresaruc(ruc);
        //Aqui se agrega que busque por concepto
        orden.Seleccionatipobusqueda(tipo);
        orden.ingresaconcepto(concepto);
        //Aqui ingresa la sucursal
        orden.ingresarsucursal(sucursal);
        orden.consultodeuda();
        orden.marcodeuda();
        orden.geraorden();
        orden.vp_mensaje();

    }
    @Test
    public void Genera_ordenPagosIEES_ConceptoSucursalCTACTE() throws IOException {

        //Ingreso de orden con perfil empresa
        Reporte.setNombreReporte("Generacion de Orden de Pago IESS - Con CTA CTE, consulta de comprobante por concepto y sucursal");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("6");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\007ordenpagoIEES.xlsx";
        //String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 0);
        String dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 1);
        String mes = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 2);
        String anio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 3);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 4);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 5);
        String ruc = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 6);
        String sucursal = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 7);
        String tipo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 8);
        String concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 4, 9);

        Ingreso_OrdenPago_IESS orden = new Ingreso_OrdenPago_IESS();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.click_link_pagoiess();
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();
        orden.ingresaruc(ruc);
        //Aqui se agrega que busque por concepto
        orden.Seleccionatipobusqueda(tipo);
        orden.ingresaconcepto(concepto);
        //Aqui ingresa la sucursal
        orden.ingresarsucursal(sucursal);
        orden.consultodeuda();
        orden.marcodeuda();
        orden.geraorden();
        orden.vp_mensaje();

    }
    @Test
    public void Genera_ordenPagosIEES_ConceptoComprobanteCTACTE() throws IOException {

        //Ingreso de orden con perfil empresa
        Reporte.setNombreReporte("Generacion de Orden de Pago IESS - Con CTA CTE, consulta de comprobante por concepto y No.comprobante");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("6");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\007ordenpagoIEES.xlsx";
        //String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 0);
        String dia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 1);
        String mes = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 2);
        String anio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 3);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 4);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 5);
        String ruc = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 6);
        //String sucursal = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 7);
        String tipo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 8);
        String concepto = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 9);
        String comprobante=excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 5, 10);

        Ingreso_OrdenPago_IESS orden = new Ingreso_OrdenPago_IESS();
        orden.click_link_pagos();
        orden.click_link_carga();
        orden.click_link_pagoiess();
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.clic_btningresar();
        orden.ingresaruc(ruc);
        //Aqui se agrega que busque por concepto
        orden.Seleccionatipobusqueda(tipo);
        orden.ingresaconcepto(concepto);
        //Aqui ingresa la comprobante
        orden.ingresacomprobante(comprobante);
        orden.consultodeuda();
        orden.marcodeuda();
        orden.geraorden();
        orden.vp_mensaje();

    }
    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }
}
