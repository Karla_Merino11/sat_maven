package RunScripts;

import Globales.Reporte;
import Globales.Util;
import TestPages.*;
import Globales.LeeExcel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.io.IOException;


public class Run_OrdenCobros_Archivo {
    WebDriver driver;

    @Before
    public void iniciar_Chrome() {
        Util.Inicio("OrdenCobro_Archivo","2");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 88.0.4324.190 (Official Build) (64-bit)"  );
    }

    @Test
    public void Genera_ordenCobrosInmediatos() throws IOException {

        //HABILITAR DRIVER DE NAVEGADOR CHROME
        Reporte.setNombreReporte("Generacion de Orden de Cobros Inmediatos exitosa - Por carga de archivo");

        LeeExcel excel = new LeeExcel();//// instanciamos la clase
        LoginSAT login = new LoginSAT();
        login.Ingresar("2");

        // Pool de datos ingreso de orden por archivo
        String filepath_pgtarjeta= System.getProperty("user.dir")+"\\DataPool\\011ordencobroSATarchivo.xlsx";
        String empresa = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 0);
        String servicio = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 1);
        String cuenta = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 2);
        String referencia = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 3);
        String carpeta=excel.getCellvalue(filepath_pgtarjeta,"Hoja1", 1,4);
        String archivo = excel.getCellvalue(filepath_pgtarjeta, "Hoja1", 1, 5);
        System.out.println(archivo);

        Ingreso_OrdenCobros_archivo orden= new Ingreso_OrdenCobros_archivo();
        orden.clic_link_cobros();
        orden.clic_link_carga();
        orden.clic_link_ingresar();
        orden.selec_empresa(empresa);
        orden.selec_servicio(servicio);
        orden.selec_cuenta(cuenta);
        orden.ingresa_referencia(referencia);
        orden.carga_archivo(carpeta,archivo);
        orden.clic_generaorden();
        orden.vp_mensaje();
        orden.clic_transmitir();
    }
    @After
    public void finaliza()
    {
        Reporte.finReporte();
    }

}
